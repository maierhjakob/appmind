//
//  ViewModel.swift
//  topmindKit
//
//  Created by Martin Gratzer on 02/09/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import Foundation
import CoreMind

public protocol ViewModelObserver: Observer {
    associatedtype ViewModel: Observable
    var viewModel: ViewModel { get set }
    func didUpdate()
//    func didUpdate(viewModel: ViewModel, key: String?)
}

extension ViewModelObserver {
    func startObserving() {
        viewModel.add(observer: self)
    }

    func stopObserving() {
        viewModel.remove(observer: self)
    }
}

open class ViewModel<Observer>: Observable {
    public typealias ObserverType = Observer
    public var weakObservers = [WeakBox]()
}
