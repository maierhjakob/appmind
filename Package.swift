// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AppMind",
    platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v3), .tvOS(.v10)
    ],
    products: [        
        .library(
            name: "AppMind",
            targets: ["AppMind"]),
    ],
    dependencies: [
        .package(url: "https://github.com/steadysense/core-mind", from: "1.0.3")
    ],
    targets: [
        .target(
            name: "AppMind",
            dependencies: ["CoreMind"]),
        .testTarget(
            name: "AppMindTests",
            dependencies: ["AppMind", "CoreMind"]),
    ]
)
